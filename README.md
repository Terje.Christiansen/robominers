# RoboMiners

Authors:
<br><br>
**Terje Christiansen**
<br>
**Tobias Setnes**


### How to run the game

* Run main/java/inf225/Main.java

## The game

For this project we decided to create a mining board-game where the player controls robots with DSL code to deal with mining problems. The DSL code will be created with the help of XText and will after further parsing be used in-game to control the robots.

**Graphics:** LibGDX
<br>
**Game Logic:** Java
<br>
**DSL:** XText (ANTLR included)
<br>
**Project:** Maven

### Experience

XText helped a lot with getting started. It creates all the tools you need for parsing and lexing and generally saves time when building your own DSL language. However, XText is not very flexible when it comes to updating the grammar. Since XText has to create all the files necessary for the language, each time one updates the grammar XText has to update all its files to fit the new grammar. One better approach than XText would be ANTLR, which XText already uses. This simplifies the process and it is easier to update the grammar without taking too much time.