package inf225.screen;
import com.badlogic.gdx.*;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import inf225.game.GameLogic;
import inf225.game.SelectRobotDetector;
import inf225.parser.RoboSpeakEvaluator;
import inf225.robot.IRobot;
import inf225.robot.Robot;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class GameScreen implements ApplicationListener {

    public static final int TILE_AREA = 300;
    private TiledMap tiledMap;
    private static TiledMapTileLayer playerLayer;

    private OrthogonalTiledMapRenderer orthogonalTiledMapRenderer;
    
    private Stage stage;
    private ArrayList<Robot> aiList;
    private GameLogic gameLogic;

    private static HashMap<IRobot,TiledMapTileLayer.Cell> robotCellHashMap;
    private ArrayList<Vector2> respawnPoints;
    private Skin skin;
    private TextArea textArea;
    
    private static GameScreen SINGLE_INSTANCE = null;
    
    private Robot currRobot;

	RoboSpeakEvaluator evaluator;
    
    
    public static GameScreen getInstance() {
        if (SINGLE_INSTANCE == null)
            SINGLE_INSTANCE = new GameScreen();

        return SINGLE_INSTANCE;
    }

	@Override
	public void create() {
        tiledMap = new TmxMapLoader().load("Maps/RoboMiners.tmx");
        orthogonalTiledMapRenderer = new OrthogonalTiledMapRenderer(tiledMap, (float) 1/TILE_AREA);
        
        this.gameLogic = new GameLogic(this);
        respawnPoints = gameLogic.getAllPositionsFromLayerName("SpawnPoint");
        robotCellHashMap = new HashMap<>();
        createAIs();
        
        playerLayer = (TiledMapTileLayer) tiledMap.getLayers().get("Player");
        TiledMapTileLayer boardLayer = (TiledMapTileLayer) tiledMap.getLayers().get("Board");
        
        robotCellHashMap = new HashMap<>();
        createAIs();
        
        stage = new Stage();
        
        skin = new Skin(Gdx.files.classpath("skin/uiskin.json"));
        textArea = new TextArea("", skin) {
    		@Override
    	    protected InputListener createInputListener () {
    	        return new TextFieldClickListener(){
    	            @Override
    	            public boolean keyDown(com.badlogic.gdx.scenes.scene2d.InputEvent event, int keycode) {
    	            	if (keycode == 57) { // ALT
    	            		System.out.println("Performing commands...");
    	            		if (currRobot != null)
								try {
									performCommand(currRobot, textArea.getText());
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
    	            	}
    	                return super.keyUp(event, keycode);
    	            };
    	        };
    	    }
    	};
    	textArea.setX(10);
    	textArea.setY(10);
    	textArea.setWidth(500);
    	textArea.setHeight(200);
    	stage.addActor(textArea);
        
        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, boardLayer.getWidth(), boardLayer.getHeight());
        camera.update();
        orthogonalTiledMapRenderer.setView(camera);
        
        setAllRespawnPoints(respawnPoints);
        
        renderRobots(getRobots());
        
        
        InputMultiplexer multiplexer = new InputMultiplexer();
		multiplexer.addProcessor(new GestureDetector(new SelectRobotDetector(this, camera)));
		multiplexer.addProcessor(stage);
		Gdx.input.setInputProcessor(multiplexer);
	}
	
	/**
     * Set all robots respawn-points
     * @param respawnPoints - respawn-points to set robots to
     */
    public void setAllRespawnPoints(ArrayList<Vector2> respawnPoints) {
        for (int i = 0; i < 4; i++) {
            getRobots().get(i).setRespawnPoint(respawnPoints.get(i));
        }
    }

	@Override
	public void render() {
		Gdx.gl.glClearColor(45/255f, 45/255f, 47/255f, 44/255f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        
        orthogonalTiledMapRenderer.render();
        
        stage.act();
        stage.draw();
	}

    /**
     * Create a chosen amount of AIs (and Cells) with chosen difficulty
     */
    public void createAIs() {
    	aiList = new ArrayList<>();
    	Robot.resetRobotID();
        int aiNumber = 4;

        for(int i = 0; i < aiNumber; i++){
            Robot newAI = new Robot();

            TiledMapTileLayer.Cell aiCell = new TiledMapTileLayer.Cell();
            aiCell.setTile(new StaticTiledMapTile(newAI.getTexture()));

            robotCellHashMap.put(newAI, aiCell);
            aiList.add(newAI);
        }
    }

    /**
     * Render all IRobots in robots-list
     * @param arrayList - list of robots to render
     */
    public void renderRobots(ArrayList<Robot> arrayList) {
        for (Robot robot : arrayList)
            renderRobot(robot);
    }

    /**
     * Get TiledMap for current map
     * @return tiledMap
     */
    public TiledMap getTiledMap() {
        return this.tiledMap;
    }

    /**
     * Update robots Cell-rotation based on Direction
     * @param robot to update cell
     */
    public void updateRobotRotation(Robot robot){
        TiledMapTileLayer.Cell cell = robotCellHashMap.get(robot);
        switch (robot.getDirection()){
            case NORTH:
                cell.setRotation(TiledMapTileLayer.Cell.ROTATE_0);
                break;
            case WEST:
                cell.setRotation(TiledMapTileLayer.Cell.ROTATE_90);
                break;
            case SOUTH:
                cell.setRotation(TiledMapTileLayer.Cell.ROTATE_180);
                break;
            case EAST:
                cell.setRotation(TiledMapTileLayer.Cell.ROTATE_270);
                break;
            default:
                break;
        }
    }

    /**
     * Change the Texture of an Image
     * @param image - Image to have drawable texture changed
     * @param texture - Texture to change to
     */
    public void changeImageTexture(Image image, Texture texture){
        image.setDrawable(new TextureRegionDrawable(new TextureRegion(texture)));
    }

    /**
     * Un-render given robot
     * @param robot - IRobot to be un-render
     */
    public void unrenderRobot(Robot robot) {
        playerLayer.setCell((int) robot.getPos().x, (int) robot.getPos().y, null);
    }

    /**
     * Render given robot with the robot's texture
     * @param robot - IRobot to be rendered
     */
    public void renderRobot(Robot robot){
    	updateRobotRotation(robot);
        playerLayer.setCell((int) robot.getPos().x, (int) robot.getPos().y, robotCellHashMap.get(robot));
    }

    @Override
    public void resize(int i, int i1) {
        stage.getViewport().update(i,i1, true);
    }

    @Override
    public void pause() {
        // pause
    }

    @Override
    public void resume() {
        // resume
    }

    @Override
    public void dispose() {
        // dispose
    }

    /**
     * Get all IRobots in game
     * @return all IRobots
     */
    public ArrayList<Robot> getRobots(){
        return aiList;
    }

    /**
     * @return this game's GameLogic
     */
    public GameLogic getGameLogic(){
        return gameLogic;
    }

	public void createCMD(int x, int y) {
		Vector2 clickPos = new Vector2(x, y);
		Robot selectedRobot = this.gameLogic.getRobotOnPos(clickPos);
		if (selectedRobot != null) {
			currRobot = selectedRobot;
			System.out.println("Currently programming: " + currRobot.getName());
		}
	}
	
	public void performCommand(Robot robot, String input) throws IOException {
		evaluator = new RoboSpeakEvaluator();
		evaluator.Evaluate(robot, input);
		textArea.setText("");
	}

	public void hasWon() {
		int totalOres = gameLogic.getAllPositionsFromLayerName("Ore").size() - 1;
		if (totalOres == 0) {
			System.out.println("YOU WON!");
			System.out.println("WOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO");
		}
	}
}