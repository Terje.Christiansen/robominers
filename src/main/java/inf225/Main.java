package inf225;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import inf225.screen.GameScreen;

import java.awt.*;

public class Main {
    public static void main(String[] args) {
        LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
        cfg.title = "RoboMiners";

        cfg.height = GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds().height;
        cfg.width = cfg.height;

        cfg.x = ((int) Toolkit.getDefaultToolkit().getScreenSize().getWidth())/2 - cfg.width/2;
        cfg.y = 0;

        new LwjglApplication(GameScreen.getInstance(), cfg);
    }
}   