package inf225.game;

import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import inf225.enums.Direction;
import inf225.robot.Robot;
import inf225.screen.GameScreen;

import static inf225.enums.Direction.*;

import java.util.ArrayList;

public class GameLogic {

    private TiledMap tiledMap;
    private GameScreen gameScreen;

    public GameLogic(GameScreen gameScreen) {
    	
    	this.gameScreen = gameScreen;
        tiledMap = gameScreen.getTiledMap();
    }

    /**
     * Checks each layer on the given pos and returns the first objectName it finds.
     * Maybe change it so it collects all objectNames on the map and returns an list of them.
     * @param tiledMap TiledMap
     * @param pos Vector2
     * @return the objectName on the pos
     */
    public String getObjectNameOnPos(Vector2 pos) {
        for (MapLayer layer : tiledMap.getLayers()) {
            for (MapObject obj : tiledMap.getLayers().get(layer.getName()).getObjects()) {
                Rectangle rect = ((RectangleMapObject) obj).getRectangle();
                int layer_x = (int) rect.x / GameScreen.TILE_AREA;
                int layer_y = (int) rect.y / GameScreen.TILE_AREA;

                if (pos.x == layer_x && pos.y == layer_y) {
                    return obj.getName();
                }
            }
        }
        return "";
    }
    
    /**
     * @param pos - position on screen
     * @return true if position is a hole or is off the map
     */
    public boolean isHole(Vector2 pos) {
        TiledMapTileLayer holeLayer = (TiledMapTileLayer) tiledMap.getLayers().get("Hole");
        TiledMapTileLayer boardLayer = (TiledMapTileLayer) tiledMap.getLayers().get("Board");
        int x = (int) pos.x;
        int y = (int) pos.y;
        return holeLayer.getCell(x,y) != null || boardLayer.getCell(x,y) == null;
    }

    /**
     * Checks if the next position in the given direction is a valid move.
     * @param pos Vector2 position
     * @param dir Direction
     * @return true if robot canGo on position
     */
    public boolean canGo(Vector2 pos, Direction dir) {
        String objectName = getObjectNameOnPos(pos);
        if (canGoCurrentTile(objectName, dir)) {
            Vector2 nextPos = getPosInDirection(pos,dir);
            objectName = getObjectNameOnPos(nextPos);

            if (objectName.contains("Diamond")) {
                return false;
            }
        }
        else {
            return false;
        }
        return true;
    }

    /**
     * Checks if the current tile is a wall in the given direction
     * @param currentObjectName object name
     * @param dir Direction
     * @return true if there is no wall in the current direction.
     */
    public boolean canGoCurrentTile(String currentObjectName, Direction dir) {
        if (currentObjectName.contains("Diamond")) {
            return !currentObjectName.toUpperCase().contains(dir.toString().toUpperCase());
        }
        return true;
    }

    /**
     * @param layerName Name of layer you want the positions from. (NOT AN OBJECT LAYER)
     * @return an ArrayList<Vector2> containing all positions of that objects on the map.
     */
    public ArrayList<Vector2> getAllPositionsFromLayerName(String layerName) {
        TiledMapTileLayer layers = (TiledMapTileLayer) tiledMap.getLayers().get(layerName);
        ArrayList<Vector2> posList = new ArrayList<>();

        for (int x = 0; x < layers.getWidth(); x++) {
            for (int y = 0; y < layers.getHeight(); y++) {
                if (layers.getCell(x, y) != null) {
                    posList.add(new Vector2(x, y));
                }
            }
        }
        return posList;
    }
    
    /**
     * Check position in direction and push robot in direction if there is a robot there
     * @param pos - pusher-robots original Vector2 position
     * @param dir - push direction
     * @return true if robot can push in direction
     */
    public boolean pushIfPossible(Vector2 pos, Direction dir){
        Vector2 vector = getPosInDirection(pos, dir);
        Robot otherRobot = getRobotOnPos(vector);

        if(otherRobot != null){
            if(!canGo(otherRobot.getPos(), dir) || !canGo(pos,dir) || !pushIfPossible(otherRobot.getPos(),dir)){
                return false;
            }

            gameScreen.unrenderRobot(otherRobot);
            otherRobot.moveOne(dir);
            gameScreen.renderRobot(otherRobot);
        }

        return true;
    }

    /**
     * @param pos - Vector2 position
     * @return IRobot-object on position or null if there is no robot on position
     */
    public Robot getRobotOnPos(Vector2 pos){
        for(Robot robot : gameScreen.getRobots()){
            if(robot.getPos().equals(pos)){
                return robot;
            }
        }
        return null;
    }

    /**
     * @param pos position you want to check
     * @return true if there is a robot on the pos
     */
    public boolean posIsOccupied(Vector2 pos) {
        return getRobotOnPos(pos) != null;
    }

    /**
     * Will push a robot to the first valid direction
     * @param pos Position you want a robot to be pushed from.
     */
    public void pushOccupyingRobot(Vector2 pos) {
        Robot robot = getRobotOnPos(pos);
        Direction dir = getValidDir(pos);

        if (dir != null && robot != null) {
            gameScreen.unrenderRobot(robot);
            robot.moveOne(dir);
            gameScreen.renderRobot(robot);
        }
    }

    private Direction getValidDir(Vector2 pos) {
        for (Direction dir : Direction.values()) {
            if (canGo(pos,dir) && pushIfPossible(pos,dir)) {
                return dir;
            }
        }
        return null;
    }


}
