package inf225.robot;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import inf225.enums.Direction;

public interface IRobot {

    /**
     * Move robot if not hindered
     * @param distance - distance robot should move
     * @param dir - direction of movement
     */
    void move(int distance, Direction dir);

    /**
     * Move robot one tile if not hindered
     * @param dir - direction of movement
     */
    void moveOne(Direction dir);

    /**
     * Rotate robot clockwise
     */
    void rotateClockwise();
    /**
     * Rotate robot counterclockwise
     */
    void rotateCounterClockwise();

    /**
     * Set robot direction
     * @param dir Direction
     */
    void setDirection(Direction dir);

    /**
     * The direction the robot is facing
     * @return Direction
     */
    Direction getDirection();

    /**
     * Load robot texture
     */
    void loadAssets();

    /**
     * Get unique texture for robot and load assets if not already loaded
     * @return texture
     */
    TextureRegion getTexture();

    /**
     * Set robot position
     * @param pos Vector2 position
     */
    void setPos(Vector2 pos);

    /**
     * Get robot position
     * @return Vector2
     */
    Vector2 getPos();

    /**
     * Get robot x position
     * @return int
     */
    int getXPos();

    /**
     * Get robot y position
     * @return int
     */
    int getYPos();

    /**
     * Set robot x position
     * @param x int
     */
    void setXPos(int x);

    /**
     * Set robot y position
     * @param y int
     */
    void setYPos(int y);

    /**
     * Get robot name
     * @return String
     */
    String getName();

	void setRespawnPoint(Vector2 vector2);
}
