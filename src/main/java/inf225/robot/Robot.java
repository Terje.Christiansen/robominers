package inf225.robot;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Vector2;

import inf225.enums.Direction;
import inf225.screen.GameScreen;

public class Robot implements IRobot {

    private int id;
    private Vector2 pos;
    private Direction direction;
    
    private Vector2 respawnPos;
    private Direction respawnDirection;

    private TextureRegion robotTexture;

    // amount of AI-robots made used for giving robots unique ID/name
    private static int aiNumber;
    
    private GameScreen gameScreen;


    public Robot(){
    	this.respawnDirection = Direction.EAST;
        this.direction = respawnDirection;
        this.id = ++aiNumber;
        this.gameScreen = GameScreen.getInstance();
    }

    /**
     * Get distance between 2 positions
     * @param pos1 Vector2 position
     * @param pos2 Vector2 position
     * @return int distance between positions
     */
    public int getDistanceBetweenPositions(Vector2 pos1, Vector2 pos2){
        return (int) (Math.abs(pos1.x - pos2.x) + Math.abs(pos1.y - pos2.y));
    }

    /**
     * Reset AI number
     */
    public static void resetRobotID(){
        aiNumber = 1;
    }

    @Override
    public void move(int distance, Direction dir){
        for (int i = 0; i < distance; i++){
            this.moveOne(dir);
        }
    }
    
    public void mine(Direction dir) {
    	Vector2 nextPos = Direction.getPosInDirection(pos, dir);
    	if (gameScreen.getGameLogic().getObjectNameOnPos(nextPos).equals("Diamond")) {
    		gameScreen.hasWon();
    		
    		((TiledMapTileLayer) gameScreen.getTiledMap().getLayers().get("Ore")).setCell((int) nextPos.x, (int) nextPos.y, null);
    		gameScreen.unrenderRobot(gameScreen.getGameLogic().getRobotOnPos(this.pos));
            setPos(nextPos);
            setDirection(dir);
            gameScreen.renderRobot(gameScreen.getGameLogic().getRobotOnPos(nextPos));
		}
    }
    
    public void moveOne(Direction dir){
        if (!(gameScreen.getGameLogic().canGo(pos, dir) && gameScreen.getGameLogic().pushIfPossible(pos,dir))) {
            System.out.println(this.getName() + " can't go");
        }else{
            Vector2 nextPos = Direction.getPosInDirection(this.pos, dir);
            if (gameScreen.getGameLogic().isHole(nextPos)) {
            	gameScreen.unrenderRobot(gameScreen.getGameLogic().getRobotOnPos(this.pos));
            	setPos(respawnPos);
            	setDirection(respawnDirection);
            	gameScreen.renderRobot(gameScreen.getGameLogic().getRobotOnPos(respawnPos));
            }
            else {
            	gameScreen.unrenderRobot(gameScreen.getGameLogic().getRobotOnPos(this.pos));
                setPos(nextPos);
                setDirection(dir);
                gameScreen.renderRobot(gameScreen.getGameLogic().getRobotOnPos(nextPos));
            }
        }
    }

    @Override
    public void rotateClockwise(){
        this.direction = Direction.rotateClockwise(direction);
    }

    @Override
    public void rotateCounterClockwise(){
        this.direction = Direction.rotateCounterClockwise(direction);
    }

    @Override
    public void setDirection(Direction dir){
        this.direction = dir;
    }

    @Override
    public Direction getDirection() {
        return this.direction;
    }

    @Override
    public void loadAssets() {
        this.robotTexture = new TextureRegion(new Texture("Robot" + (((this.id + 1) % 3) + 2)  + ".png"));
    }

    @Override
    public TextureRegion getTexture() {
        if (robotTexture == null) {
            loadAssets();
        }
        return this.robotTexture;
    }

    @Override
    public void setPos(Vector2 pos) {
        this.pos = pos;
    }

    @Override
    public Vector2 getPos() { return pos; }

    @Override
    public int getXPos() {
        return (int) pos.x;
    }

    @Override
    public int getYPos() {
        return (int) pos.y;
    }

    @Override
    public void setXPos(int x) {
        this.setPos(new Vector2(x, this.getYPos()));
    }

    @Override
    public void setYPos(int y) {
        this.setPos(new Vector2(this.getXPos(), y));
    }

    @Override
    public String getName() {
        return "ROBOT" + this.id;
    }

	@Override
	public void setRespawnPoint(Vector2 pos) {
		this.respawnPos = pos;
		setPos(respawnPos);
	}
}
