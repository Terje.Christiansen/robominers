package inf225.parser;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.inf225.RoboSpeakStandaloneSetupGenerated;
import org.inf225.roboSpeak.Mine;
import org.inf225.roboSpeak.Model;
import org.inf225.roboSpeak.Move;
import org.inf225.roboSpeak.Query;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import inf225.robot.Robot;
import inf225.enums.Direction;

import com.google.inject.Injector;

public class RoboSpeakEvaluator {
	
	public RoboSpeakEvaluator() {
	}
	
	public List<String> tokenize(String input) {
		List<String> tokens = new ArrayList<>();
		StringTokenizer tokenizer = new StringTokenizer(input);
		while (tokenizer.hasMoreElements()) {
			tokens.add(tokenizer.nextToken());
		}
		return tokens;
	}
	
	
	public void Evaluate(Robot robot, String input) throws IOException {
		
		@SuppressWarnings("unused")
		File parseThis = new File("parseThis.rbspk");
		FileWriter myWriter = new FileWriter("parseThis.rbspk");
	    myWriter.write(input);
	    myWriter.close();
	    Injector injector = new RoboSpeakStandaloneSetupGenerated().createInjectorAndDoEMFRegistration();
		XtextResourceSet resourceSet = injector.getInstance(XtextResourceSet.class);
		org.eclipse.emf.ecore.resource.Resource resource = resourceSet.getResource(URI.createFileURI("./parseThis.rbspk"), true);
		Model model = (Model) resource.getContents().get(0);
		EList<Query> commands = model.getCommands();
		for (int i = 0; i < (commands.size()); i++) {
			Query currCommand = commands.get(i);
			if (currCommand instanceof Move) {
				Move currCommandMove = (Move) currCommand;
				Direction dir = Direction.getDir(currCommandMove.getMoveDir());
				if (dir == null) {
					System.out.println("Illegal Argument for Move");
					return;
				}
				int dist = Integer.valueOf(currCommandMove.getDist());
				if (dist < 0) {
					robot.move(-dist, Direction.oppositeOf(dir));
				}
				robot.move(dist, dir);
			}
			else if (currCommand instanceof Mine) {
				Mine currCommandMine = (Mine) currCommand;
				Direction dir = Direction.getDir(currCommandMine.getMineDir());
				if (dir == null) {
					System.out.println("Illegal Argument for Mine");
					return;
				}
				robot.mine(dir);
			}
			System.out.println("move number " + (i+1) + ":");
			System.out.println(model.getCommands().get(i));
		}
	}
	
}
